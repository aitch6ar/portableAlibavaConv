CC=gcc
CXX=g++ -std=c++11
RM=rm -f
CPPFLAGS=-g $(shell root-config --cflags)
LDFLAGS=-g $(shell root-config --ldflags)
LDLIBS=$(shell root-config --libs) -lRooFitCore
EXECUTABLE=alibava-convert

SRCS=main.cpp convert.cpp correct.cpp calibration.cpp fwhm.cpp 
OBJS=$(subst .cpp,.o,$(SRCS))

all: $(EXECUTABLE)

$(EXECUTABLE): $(OBJS)
	$(CXX) $(LDFLAGS) -o $(EXECUTABLE) $(OBJS) $(LDLIBS) 

depend: .depend

.depend: $(SRCS)
	rm -f ./.depend
	$(CXX) $(CPPFLAGS) -MM $^>>./.depend;

clean:
	$(RM) $(OBJS) *~ .depend

install:
	alias $(EXECUTABLE)=.$(shell pwd)/$(EXECUTABLE)

include .depend
