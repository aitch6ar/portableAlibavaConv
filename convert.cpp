#include <iostream>
#include <fstream>
#include <TString.h>
#include <TTree.h>
#include <TFile.h>
#include <TH2F.h>
#include <TCanvas.h>
#include <TColor.h>
#include <TStyle.h>

using namespace std;

TString convert(TString filename){
	const unsigned int nChan=256;
	Int_t filesize = 1200*1024*1024; // sub file size, bytes
	time_t timestart, timestop;
	Int_t bunchsize, runType, fileSizeInB;
	unsigned int clock;
	unsigned int timeBin;
		unsigned short timeFPart;
		short timeIPart;
		Float_t time;
	unsigned short int tempBin;
	Float_t temp;
	UShort_t amp[nChan];
	double aprioripedestals[nChan];
	double apriorinoise[nChan];
	double charge;
	unsigned int headerLength, headerDB;
	TString header, filenametemp;
	char letter;
	TString runTypeName[5] = {      "cc",
					"lc",
					"lr",
					"rs",
					"pd",
	};

	ifstream ifile;
	ifile.open(filename);
	if (!(ifile.is_open())){
		printf("Can not open \"%s\", file does not exist\n ",filename.Data());
		return "";
	}
	else printf("Processing \x1b[94m%s\x1b[0m...\n",filename.Data());

	filename.ReplaceAll(".bin","");
	TString filetitle = filename;
	filetitle.Remove(0,(filename.Last('/')+1));


	system(Form("rm -rf %s",filename.Data()));
	system(Form("mkdir %s",filename.Data()));


	TH2F* histAmpCh = new TH2F("histAmpCh","",256,0.,256.,1024,0.,1024.);
	histAmpCh->SetXTitle("Channel #");
	histAmpCh->SetYTitle("Amplitude, ADC units");

	TFile* hfile = TFile::Open(filename+"/rawdata_"+filetitle+".root","new");
	
	TTree *treeData = new TTree("treeData","treeData");
	treeData->SetMaxTreeSize(filesize);
	treeData->Branch("charge",&charge,"charge/D");
	treeData->Branch("clock",&clock,"clock/i");
	treeData->Branch("time",&time,"time/F");	
	treeData->Branch("temp",&temp,"temp/F");
	treeData->Branch("amp",&amp,"amp[256]/s");
	
	ifile.read((char *)& timestart, sizeof(time_t));
	ifile.read((char *)& runType, sizeof(int));
	ifile.read((char *)& headerLength, sizeof(unsigned int));
	for(int i=0;i<headerLength;i++){
		ifile.read((char *)& letter, sizeof(char));
		header.Append(letter);
	}
	printf("%s %s",header.Data(),ctime(&timestart));
	header.Remove(0, header.Index("|")+1);
	Int_t totEv = header.Atoi();
	header.Remove(0, header.Index(";")+1);
	bunchsize = header.Atoi();
	

	int nEv=0;
	printf("A binary file is being converted to the ROOT format...\n");
	while(true){
//		if (nEv == 1e4) break;
		if(ifile.eof()) break;
		if(!(ifile.read((char *)& headerDB, sizeof(unsigned int)) )) break;
		ifile.seekg(1*sizeof(unsigned int),ios::cur);
		if(ifile.eof()) break;
		ifile.read((char *)& charge, sizeof(double));
		charge =  (charge-5570559)*1000;
		ifile.read((char *)& clock, sizeof(unsigned int));
		ifile.read((char *)& timeBin, sizeof(unsigned int));
		time = 100.0*(abs((timeBin & 0xffff0000)>>16) + ((timeBin & 0xffff)/65535.));
		ifile.read((char *)& tempBin, sizeof(unsigned short int));
		temp = 0.12*tempBin-39.8;
		ifile.seekg(32,ios::cur);
		ifile.read((char *)& amp, 128*sizeof(unsigned short int));
		ifile.seekg(32,ios::cur);
		ifile.read((char *)& amp[128], 128*sizeof(unsigned short int));
		for(int i=0;i<nChan;i++){
			histAmpCh->Fill(i+.5,amp[i]);
		}
		treeData->Fill();
		nEv++;
		if(!(nEv%1000)&&runType!=1){
			printf("\33[2K"); // delete content of the current string
			printf("Events converted:\t%i/%i\t%5.1f\%\n",nEv, totEv, 100*nEv/float(totEv));
			printf("\033[A"); // return one string up
		}
	}
	Float_t chipTemp =-10., chipTempErr=2;
	if(runType!=1){
		printf("\n");
		TH1F* histTemperature = new TH1F("histTemperature","",1e3,-40,60);
		treeData->Project("histTemperature","temp");
		chipTemp = histTemperature->GetMean();
		chipTempErr = histTemperature->GetRMS();
		printf("Chip temperature:\t%+.1f+-%.1f C\n",chipTemp,chipTempErr);
	}

	hfile = treeData->GetCurrentFile();
	hfile->Write();
	hfile->Close();
	delete hfile;
	hfile = TFile::Open(filename+"/rawdata_"+filetitle+".root", "update");
	histAmpCh->Write();
	hfile->Close();
	printf("Output files are stored in \e[94m%s\e[0m\n",filename.Data());

	TCanvas canvA("canvA","canvA",1000,600);
	bool side=true;
	canvA.SetLogz();
	gStyle->SetOptStat(0);
//	bool side = .5*(bias/sqrt(pow(bias,2)))+1;
	Double_t colors[4][3] = {
	        {0.0, 0.4, 1.0},
	        {1.0, 0.8*!side, 0.3},
	        {1.0, 0.8*side, 0.3},
	        {0.3, 0.0, 1.0}
	};
	TColor::CreateGradientColorTable(3,colors[0],colors[1],colors[2],colors[3],256);
	histAmpCh->GetXaxis()->SetNdivisions(-808);
	histAmpCh->GetYaxis()->SetNdivisions(-808);
	histAmpCh->Draw("colz");
	canvA.SaveAs(Form("%s/rawdata_%s.png",filename.Data(),filetitle.Data()));

	filename += runTypeName[runType];
	return filename;
























}
