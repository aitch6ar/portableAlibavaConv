#include <iostream>
#include <stdlib.h>
#include <TString.h>
#include <fstream>
#include "convert.h"
#include "correct.h"
#include "calibration.h"
#include <TStyle.h>
#include <TError.h>

using namespace std;

int main(int argc, char **argv){
	gStyle->SetOptStat(0);
	gErrorIgnoreLevel = kWarning;
	for(int i=1;i<=argc;i++){
		TString filename = argv[i];
		if(!(filename.EndsWith(".bin"))) continue;
		filename = convert(filename);
		if(filename.IsNull()) printf("Corrupted file, convertion is terminated!");
		if(filename.EndsWith("pd")||filename.EndsWith("rs")){
			filename.Remove(filename.Length()-2,2);
			correct(filename);
		}
		if(filename.EndsWith("cc")){
			filename.Remove(filename.Length()-2,2);
			calibration(filename);
		}
	}
	return 0;

}
