#include <unistd.h>
#include <iostream>
#include "fwhm.h"
#include <TString.h>
#include <TChain.h>
#include <TTree.h>
#include <TChain.h>
#include <TFile.h>
#include <TSystem.h>
#include <TGraphErrors.h>
#include <TMath.h>
#include <TH2F.h>
#include <TCanvas.h>
#include <TColor.h>
#include <TPad.h>
#include <TLegend.h>
	

using namespace std;

void correct(TString filename){
	const int nChan=256, nInt=4;
	const int filesize = 1200*1024*1024; // sub file size, bytes	
	bool side=true;
	Int_t nevents;
	Float_t time, temp;
	Int_t clock;
	UShort_t amp[nChan];
	Float_t pedestals[nChan], noise[nChan], event[nChan], median[nChan/nInt], variation[nChan/nInt];

	printf("Baseline correction...\n");
	TString filetitle = filename;
	TString directory = filename;
	if(filename.Contains('/')){
		directory.Remove(directory.Last('/'),directory.Length()-directory.Last('/'));
		filetitle.Remove(0,filetitle.Last('/')+1);
		directory+='/';
	} else {
		directory="";
	}


	TChain* treeData = new TChain("treeData");
	filename = Form("%s%s/rawdata_%s",directory.Data(),filetitle.Data(), filetitle.Data());
	treeData->Add(filename+".root");
//	cout << filename+".root" << endl;
	TFile* infile = TFile::Open(filename+".root","read");
	TH2F* histAmpCh = (TH2F*) infile->Get("histAmpCh");
	int i=0;
	while(true){
		i++;
		if(gSystem->AccessPathName(Form("%s_%i.root",filename.Data(),i))) break;
		cout << Form("%s_%i.root",filename.Data(),i) << endl;
		treeData->Add(Form("%s_%i.root",filename.Data(),i));
	}
	treeData->SetBranchAddress("amp",amp);
	treeData->SetBranchAddress("clock",&clock);
	treeData->SetBranchAddress("time",&time);
	treeData->SetBranchAddress("temp",&temp);
	nevents = treeData->GetEntries();
//	cout << nevents << endl;


	TH2F* histAmpCh_zc = new TH2F("histAmpCh_zc","histAmpCh_zc",nChan,0,nChan,1024,-512,512);
	TH2F* histAmpCh_bc = new TH2F("histAmpCh_bc","histAmpCh_bc",nChan,0,nChan,1024,-512,512);
	histAmpCh_bc->SetXTitle(histAmpCh->GetXaxis()->GetTitle());
	histAmpCh_bc->SetYTitle("Absolute amplitude, ADC units");
	histAmpCh_bc->SetTitle("");

	system(Form("rm -f %s/%s/pedsubs_%s*",directory.Data(),filetitle.Data(),filetitle.Data()));
	filename.ReplaceAll("rawdata_","pedsubs_");
	TFile* outfile = TFile::Open(filename+".root","recreate");
//	TTree* treeParamNew = treeParam->CloneTree();

	TH1F* histNoiseFWHM = new TH1F("histNoiseFWHM","noise/ch.",nChan,0,nChan);
	TH1F* histNoiseFWHM_bc = new TH1F("histNoiseFWHM_bc","noise/ch., baseline corrected",nChan,0,nChan);
	TH1F* histNoiseRMS = new TH1F("histNoiseRMS","histNoiseRMS",nChan,0,nChan);
	TH1F* histNoiseRMS_bc = new TH1F("histNoiseRMS_bc","histNoiseRMS_bc",nChan,0,nChan);
	histNoiseFWHM->SetLineColor(kMagenta);
	histNoiseFWHM_bc->SetLineColor(kBlue);

	TH1D* histAmp;
	float centre, sigma;
	TGraphErrors* graphPedFWHM = new TGraphErrors(nChan);
	graphPedFWHM->SetName("graphPedFWHM");
	for(int i=0;i<nChan;i++){
		histAmp = (TH1D*) histAmpCh->ProjectionY("histAmp",i+1,i+1);
		fwhm(histAmp, &centre, &sigma);
		pedestals[i] = centre;
		noise[i] = sigma;
		graphPedFWHM->SetPoint(i,i+.5,centre);
		graphPedFWHM->SetPointError(i,.5,sigma);
		histNoiseFWHM->SetBinContent(i+1,sigma);
		delete histAmp;
	}

	TTree* treeDataCorrected = new TTree("treeData_bc","treeData_bc");
	treeDataCorrected->SetMaxTreeSize(filesize);
	treeDataCorrected->Branch("clock",&clock,"clock/I");
	treeDataCorrected->Branch("time",&time,"time/F");
	treeDataCorrected->Branch("temp",&temp,"temp/F");
	treeDataCorrected->Branch("event",event,"event[256]/F");
	treeDataCorrected->Branch("median",median,"median[4]/F");
	treeDataCorrected->Branch("variation",variation,"variation[4]/F");

	TString filenameBias = filename;
	filenameBias.Remove(0,filenameBias.Last('/'));
	filenameBias.Remove(0,filenameBias.Index("bias"+4));
	Float_t bias = filenameBias.Atof();
	side = 1+.5*bias/sqrt(pow(bias,2));
	if(filename.Contains("p-side")) side=true;
	if(filename.Contains("n-side")) side=false;

	for(int i=0;i<nevents;i++){
		treeData->GetEntry(i);
		for(int j=0;j<nChan;j++){
			event[j]=float(amp[j])*2*(side-.5);
			event[j]-=pedestals[j]*2*(side-.5);
			histAmpCh_zc->Fill(j+.5,event[j]);
		}	
		for(int j=0;j<4;j++){
			median[j] = TMath::Median(nChan/nInt,&(event[j*nChan/nInt]));
			variation[j] = TMath::RMS(nChan/nInt,&(event[j*nChan/nInt]));
		}
		for(int j=0;j<nChan;j++){
			event[j]-=median[j/64];
			histAmpCh_bc->Fill(j+.5,event[j]);
		}
		treeDataCorrected->Fill();
		if(!((i+1)%1000)){
			printf("\33[2K"); // delete content of the current string
			printf("Events processed:\t%i/%i\t%5.1f\%\n",i+1, nevents, 100*(i+1)/float(nevents));
			printf("\033[A"); // return one string up
		}
	}
	outfile = treeDataCorrected->GetCurrentFile();
	outfile->Write();

	printf("\n");

	TGraphErrors* graphPedFWHM_bc = new TGraphErrors(nChan);
	graphPedFWHM_bc->SetName("graphPedFWHM_bc");
	for(int i=0;i<nChan;i++){
		histAmp = (TH1D*) histAmpCh_bc->ProjectionY("histAmp",i+1,i+1);
		fwhm(histAmp, &centre, &sigma);
		graphPedFWHM_bc->SetPoint(i,i+.5,centre);
		graphPedFWHM_bc->SetPointError(i,.5,sigma);
		histNoiseFWHM_bc->SetBinContent(i+1,sigma);
		delete histAmp;
	}

	for(int i=0;i<nChan;i++){
		histNoiseRMS->SetBinContent(i+1,(histAmpCh->ProjectionY("histAmp",i+1,i+1)->GetRMS()));		
		histNoiseRMS_bc->SetBinContent(i+1,(histAmpCh_bc->ProjectionY("histAmp_bc",i+1,i+1)->GetRMS()));		
	}


	outfile->Open(Form("%s.root",filename.Data()),"update");
	histAmpCh->Write();
	histAmpCh_bc->Write();
	graphPedFWHM->Write();	
	graphPedFWHM_bc->Write();	
	histNoiseFWHM->Write();
	histNoiseFWHM_bc->Write();
	histNoiseRMS->Write();
	histNoiseRMS_bc->Write();


// drawing:	
	TCanvas canvA("canvA","canvA",1000,600);
	canvA.SaveAs(Form("%s.pdf[",filename.Data()));

	Double_t colors[4][3] =	{
		{0.0, 0.4, 1.0},
		{1.0, 0.8*!side, 0.3},
		{1.0, 0.8*side, 0.3},
		{0.3, 0.0, 1.0}
	};
	TColor::CreateGradientColorTable(3,colors[0],colors[1],colors[2],colors[3],256);
	gPad->SetGridx();
	gPad->SetLogz();
	histAmpCh->GetXaxis()->SetNdivisions(-808);
	histAmpCh->GetYaxis()->SetNdivisions(-808);
	histAmpCh->Draw("colz");
	canvA.SaveAs(Form("%s.pdf",filename.Data()));
	histAmpCh_bc->GetXaxis()->SetNdivisions(-808);
	histAmpCh_bc->GetYaxis()->SetNdivisions(-808);
	histAmpCh_bc->Draw("colz");
	canvA.SaveAs(Form("%s.pdf",filename.Data()));

	TH1F* frameNoise = canvA.DrawFrame(0,0,float(nChan),int(1.2*(TMath::Max(histNoiseFWHM->GetMaximum(),histNoiseFWHM_bc->GetMaximum()))));
	frameNoise->SetXTitle(histAmpCh->GetXaxis()->GetTitle());
	frameNoise->SetYTitle("Noise, ADC units");
	frameNoise->GetXaxis()->SetNdivisions(-808);
	frameNoise->Draw();
	histNoiseFWHM->Draw("same");
	histNoiseFWHM_bc->Draw("same");
	TLegend* legendNoise = new TLegend(.55,.75,.85,.85);
	legendNoise->SetLineColor(kWhite);
	legendNoise->SetFillColor(kWhite);
	legendNoise->AddEntry(histNoiseFWHM->GetName(),histNoiseFWHM->GetTitle(),"l");
	legendNoise->AddEntry(histNoiseFWHM_bc->GetName(),histNoiseFWHM_bc->GetTitle(),"l");
	legendNoise->Draw();
	canvA.SaveAs(Form("%s.pdf",filename.Data()));


	TCanvas canvB("canvB","canvB",2000,2000);
	canvB.Divide(4,4);
	TH1F* frameChannel;
	for(int i=0;i<nChan;i++){
		canvB.cd(i%16+1);
		gPad->SetLogy();
		frameChannel = gPad->DrawFrame(-40,1,100,(3.3*(TMath::Max(histAmpCh_zc->ProjectionY("histAmp_zc",i+1,i+1)->GetMaximum(),histAmpCh_bc->ProjectionY("histAmp_bc",i+1,i+1)->GetMaximum()))));
		frameChannel->SetXTitle("Amplitude, ADC units");
		frameChannel->SetYTitle("Entries/ADC unit");
		frameChannel->SetTitle(Form("Channel #%i",i));
		frameChannel->Draw();
		histAmp = histAmpCh_zc->ProjectionY("histAmp_zc",i+1,i+1);
		histAmp->SetLineColor(kMagenta);
		histAmp->DrawCopy("same");
		histAmp = histAmpCh_bc->ProjectionY("histAmp_bc",i+1,i+1);
		histAmp->SetFillColor(kCyan);
		histAmp->DrawCopy("same");
		if(!((i+1)%16)) canvB.SaveAs(Form("%s.pdf",filename.Data()));
	}
	canvB.SaveAs(Form("%s.pdf",filename.Data()));


	canvA.SaveAs(Form("%s.pdf]",filename.Data()));

//	usleep(5e6);
	printf("Done!\n");





}
