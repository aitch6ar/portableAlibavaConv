
#include <TH1F.h>
#include <TGraph.h>
#include <TF1.h>

using namespace std;

float fwhm(TH1D* hist, float* centre=0, float* sigma=0){
	hist->Smooth(4);
	TGraph* graphAmp = new TGraph(hist);
	int nbins= hist->GetNbinsX();
	float ledge, redge;
	ledge = hist->GetBinLowEdge(1);
	redge = hist->GetBinLowEdge(nbins)+hist->GetBinWidth(nbins);
	TF1* funcAmp = new TF1("function_amplitude",[&](double*x, double *p){ return graphAmp->Eval(x[0]); }, ledge, redge, 1);
//	cout << nbins << endl << hist->GetBinLowEdge(1) << endl << hist->GetBinLowEdge(nbins)+hist->GetBinWidth(nbins) << endl;
	float xlo, xhi, xc;	
		xc = funcAmp->GetMaximumX(ledge,redge);
		xlo = funcAmp->GetX(.5*(funcAmp->GetMaximum(ledge,redge)),ledge,xc);
		xhi = funcAmp->GetX(.5*(funcAmp->GetMaximum(ledge,redge)),xc,redge);
	if(centre){
		*centre = .5*(xhi+xlo);
	}
	if(sigma){
		*sigma = (xhi-xlo)/2.355;
	}
	return (xhi-xlo);


}
