#include <unistd.h>
#include <iostream>
#include "fwhm.h"
#include <TString.h>
#include <TChain.h>
#include <TTree.h>
#include <TChain.h>
#include <TFile.h>
#include <TSystem.h>
#include <TGraphErrors.h>
#include <TMath.h>
#include <TH2F.h>
#include <TCanvas.h>
#include <TColor.h>
#include <TPad.h>
#include <TLegend.h>
#include <TLatex.h>
#include <RooRealVar.h>
#include <RooArgSet.h>
#include <RooArgList.h>
#include <RooFormulaVar.h>
#include <RooDataSet.h>
#include <RooPlot.h>

using namespace std;
using namespace RooFit;

void calibration(TString filename){
	const int nChan=128;
	const int filesize = 1200*1024*1024; // sub file size, bytes	
	Int_t nevents;
	UShort_t amp[nChan];
	Double_t charge;
	UShort_t ampVec[nChan][2][50];
	
	

	printf("Internal charge calibration...\n");
	TString filetitle = filename;
	TString directory = filename;
	if(filename.Contains('/')){
		directory.Remove(directory.Last('/'),directory.Length()-directory.Last('/'));
		filetitle.Remove(0,filetitle.Last('/')+1);
		directory += '/';
	} else {
		directory = "";
	}
	bool side=true;
	if(filename.Contains("p-side")) side=true;
	if(filename.Contains("n-side")) side=false;


	TChain* treeData = new TChain("treeData");
	filename = Form("%s/%s/rawdata_%s",directory.Data(),filetitle.Data(), filetitle.Data());
	treeData->Add(filename+".root");
//	cout << filename+".root" << endl;
	TFile* infile = TFile::Open(filename+".root","read");
	TH2F* histAmpCh = (TH2F*) infile->Get("histAmpCh");
	int i=0;
	while(true){
		i++;
		if(gSystem->AccessPathName(Form("%s_%i.root",filename.Data(),i))) break;
		cout << Form("%s_%i.root",filename.Data(),i) << endl;
		treeData->Add(Form("%s_%i.root",filename.Data(),i));
	}
	treeData->SetBranchAddress("amp",amp);
	treeData->SetBranchAddress("charge",&charge);
	nevents = treeData->GetEntries();
	TH2F* histCal = new TH2F("histCal","",nChan,0,float(nChan),64,-32,32);
	histCal->GetXaxis()->SetNdivisions(-808);
	histCal->SetXTitle("Channel #");
	histCal->SetYTitle("Charge, ke");
	histCal->SetZTitle("Amplitude, ADC units");
	histCal->GetXaxis()->SetTitleOffset(1.5);
	histCal->GetYaxis()->SetTitleOffset(1.8);
	histCal->GetZaxis()->SetTitleOffset(1.2);
	for(int i=0;i<nevents;i++){
		treeData->GetEvent(i);
		charge/=1e3;
		for(int j=0;j<nChan;j++){
			ampVec[j][(i+j)%2][(i%100)/2] = amp[j];
		}
		if(i%100){
			for(int j=0;j<nChan;j++){
				histCal->SetBinContent(j+1,i/100+1+32,TMath::Mean(50,ampVec[j][1]));
				histCal->SetBinContent(j+1,-i/100+32,TMath::Mean(50,ampVec[j][0]));
				histCal->SetBinError(j+1,i/100+1+32,(1/sqrt(50))*TMath::RMS(50,ampVec[j][1]));
				histCal->SetBinError(j+1,-i/100+32,(1/sqrt(50))*TMath::RMS(50,ampVec[j][0]));
			}
		}


	}
	system(Form("rm -f %s/%s/calibrc_%s*",directory.Data(),filetitle.Data(),filetitle.Data()));

// drawing:
	filename.ReplaceAll("rawdata_","calibrc_");
	TCanvas canvA("canvA","canvA",1000,600);
	canvA.SaveAs(Form("%s.pdf[",filename.Data()));
	histCal->Draw("surf2");
	canvA.SaveAs(Form("%s.pdf",filename.Data()));

// fitting:
	bool drawChannels = false;
	TGraphErrors* grGain = new TGraphErrors(nChan);
	TGraphErrors* grPedestal= new TGraphErrors(nChan);


	RooRealVar varCharge("varCharge","Charge",-34,34,"ke");
	RooRealVar varADC("varADC","gain",0,1024,"ADC units");
	RooArgSet setVar(varCharge,varADC);
	RooDataSet* dsetCalibr;
	RooPlot* frameCalibr;
	RooRealVar varSlope("varSlope","varSlope",0,-100,100);
	RooRealVar varOffset("varOffset","varOffset",512,0,1024);
	RooFormulaVar* funcLine = new RooFormulaVar("funcLine","funcLine","@2+@0*@1",RooArgList(varCharge,varSlope,varOffset));
	
	for(int i=0;i<nChan;i++){
		printf("\33[2K");
		printf("Channels calibrated:\t%i/%i\n",i+1, nChan);
		printf("\033[A"); // return one string up

		dsetCalibr = new RooDataSet("dsetCalibr","dsetCalibr",setVar,StoreError(setVar));
		frameCalibr = varCharge.frame();
		frameCalibr->SetTitle(Form("Channel #%i",i));
		frameCalibr->SetYTitle("Amplitude (ADC counts)");
		for(int j=-32;j<32;j++){
			if(j<0) varCharge.setVal(j);
			else varCharge.setVal(j+1);
			varCharge.setError(.5);
			varADC.setVal(histCal->GetBinContent(i+1,j+32+1));
			varADC.setError(histCal->GetBinError(i+1,j+32+1));
			dsetCalibr->add(setVar);
		}
		funcLine->chi2FitTo(*dsetCalibr,YVar(varADC),PrintLevel(-1));
		if(drawChannels){
			dsetCalibr->plotOnXY(frameCalibr,YVar(varADC));
			funcLine->plotOn(frameCalibr);
			frameCalibr->Draw();
			canvA.SaveAs(Form("%s.pdf",filename.Data()));
		}
		delete dsetCalibr;
		delete frameCalibr;
		
		grGain->SetPoint(i,i+.5,varSlope.getVal());
		grGain->SetPointError(i,0,varSlope.getError());
		grPedestal->SetPoint(i,i+.5,varOffset.getVal());
		grPedestal->SetPointError(i,0,varOffset.getError());
	}
	printf("\n");

// mean gain evaluation:
	Float_t gainWindow = .2;
	TH1F* histMeanGain = new TH1F("histMeanGain","histMeanGain",200,0,20);
	Double_t* vecGain = grGain->GetY();
	Float_t medianGain = TMath::Median(128,vecGain);
	for(int i=0;i<nChan;i++){
		if(vecGain[i]>((1-gainWindow)*medianGain)&&vecGain[i]<((1+gainWindow)*medianGain)) histMeanGain->Fill(vecGain[i]);
	}
	Float_t meanGain = histMeanGain->GetMean();
	Float_t meanGainErr = histMeanGain->GetRMS();
	
	


// drawing again:
	
	TH1F* frameGraph = canvA.DrawFrame(0,0,128.,12.);
	frameGraph->SetTitle("");
	frameGraph->GetXaxis()->SetNdivisions(-808);
	frameGraph->SetXTitle("Channel #");
	frameGraph->SetYTitle("Gain, ADC units/ke^{-}");
	frameGraph->Draw();
	grGain->Draw("same, p");
	TLatex* latexBox = new TLatex();
	latexBox->SetTextSize(.03);
	latexBox->DrawLatexNDC(.15,.80,Form("Median gain: %.1f ADC units/ke^{_}",TMath::Median(128,grGain->GetY()) ));
	latexBox->DrawLatexNDC(.15,.75,Form("Mean gain: %.1f #pm %.1f ADC units/ke^{_}",meanGain,meanGainErr ));
	canvA.SaveAs(Form("%s.pdf",filename.Data()));
	delete frameGraph;

	canvA.Clear();
	frameGraph = canvA.DrawFrame(0,512-128,128.,512+128);
	frameGraph->SetTitle("");
	frameGraph->GetXaxis()->SetNdivisions(-808);
	frameGraph->GetYaxis()->SetNdivisions(-808);
	frameGraph->SetXTitle("Channel #");
	frameGraph->SetYTitle("Pedestals, ADC units");
	frameGraph->Draw();
	grPedestal->Draw("same, p");
	canvA.SaveAs(Form("%s.pdf",filename.Data()));












	canvA.SaveAs(Form("%s.pdf]",filename.Data()));



	delete histAmpCh;
	printf("Done!\n");


}
